# Car Rental Service

<!-- TOC -->
* [Car Rental Service](#car-rental-service)
  * [Projects structure](#projects-structure)
  * [Documentation](#documentation)
  * [Requirements to run the service:](#requirements-to-run-the-service)
  * [Running the service](#running-the-service)
  * [Run Postgresql from docker manually](#run-postgresql-from-docker-manually)
    * [Connecting to the database](#connecting-to-the-database)
    * [Flyway](#flyway)
  * [Debugging](#debugging)
  * [Running tests](#running-tests)
  * [When implementing new changes](#when-implementing-new-changes)
<!-- TOC -->

## Projects structure

Car rental service is an application exposing REST API endpoints for CRUD operations with vehicle
data. Application manipulates with basic data about what a typical car rental website would need:

- Returns data about vehicles, accidents, subscriptions, repairs etc. with supported pagination and other query parameters
- Includes an admin privilege to access create, update and delete operations
- Lets you add new repairs, update discounts and delete vehicles

The project consists of two modules:

[Logic module](car-rental-service-logic) :
Module consisting of all business logic, which includes REST controllers implementation, service layer and a persistence layer.

[REST module](car-rental-service-rest-client) :
Module consisting of REST controllers interfaces with added Swagger documentation, DTOs (Data Transfer Objects) and exceptions. This module
should be a full-fledged dependency (if deployed) that could be used together with [Spring Cloud OpenFeign](https://spring.io/projects/spring-cloud-openfeign/)
to send request to from another service (if used with [Eureka Server](https://cloud.spring.io/spring-cloud-netflix/reference/html/)).

## Documentation
All REST endpoints are documented in [openapi.yaml](openapi.yaml) or can be accessed on a [localhost page](http://localhost:8080/swagger-ui/index.html).

## Requirements to run the service:
1. [JDK 17](https://jdk.java.net/archive/) - Do not develop in different versions. You can also install JDK via IntelliJ IDEA.
2. [Docker](https://www.docker.com/products/docker-desktop/) - Needed to run tests and database locally.

## Running the service

- [Clone](https://gitlab.fel.cvut.cz/B231_B6B36EAR/figuldan) the repository and run Intellij IDEA.
- Go to _File > Project structure_ or alternatively press <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>S</kbd> and set SDK to 17 and language level to 17:

![sdkjdk.png](pic/readme/sdkjdk.png)

- Before you run your service, you have to generate additional classes via `mvn clean package` command and click to first two icons
  in Maven window to load project and then generate mappers, etc.
- Alternatively, you can start `mvn clean` and `mvn package` via IntelliJ IDE:

![mvnpackage.png](pic/readme/mvncleanpackage.png)

- Run the service. Docker compose for PostgreSQL should be run automatically for you, or you may [run it
manually](#run-postgresql-from-docker-manually) before running the service

## Run Postgresql from docker manually

- Start Docker compose with this command: 
```shell
docker compose -f docker-compose-postgresql.yaml up
```
  First run of docker can fail, second one should be ok.
- If something is wrong with you database, you can remove volume with it: 
```shell
docker compose -f docker-compose-postgres.yaml down -v
```
- Alternatively, you can start the compose via IntelliJ IDEA GUI:

![dockercomposerun.png](pic/readme/dockercomposerun.png)

### Connecting to the database

You can connect to the created database (via IntelliJ IDEA database, DataGrip, pgAdmin...) by setting 
:

![datagripconnect.png](pic/readme/datagripconnect.png)

You can view the properties for connecting to the database in [PostgreSQL Docker compose file](docker-compose-postgresql.yaml)

### Flyway

The service uses [Flyway](https://flywaydb.org/) - a database-migration tool that we use to fill database with the data upon service start.
It runs on the first run of the application, and it creates a **flyway_schema_history** table that saves
checksums for each migration script. All migration scripts can be found in 
[projects resources](car-rental-service-logic/src/main/resources/db)

## Debugging

If the app run detects some errors, it can be due to several reasons. The most common reasons are:

- You didn't generate classes. Run `mvn clean package.`
- You don't have Docker or container of the service running.
- You already have a different service started on port `8080` or you already started another instance of the app.

## Running tests

You need a running Docker in order to run tests, they use [Testcontainers](https://testcontainers.com/).
Testcontainers is a framework used to activate automatic startup and stop of containers used in a test case.
Thanks to this, we achieve true **ACID** properties of a database in a test environment, ensuring total isolation from our 'real' data.

## When implementing new changes

After almost every change you should repackage (regenerate) the classes with maven (`mvn clean package`). After that you can run and try your changes.