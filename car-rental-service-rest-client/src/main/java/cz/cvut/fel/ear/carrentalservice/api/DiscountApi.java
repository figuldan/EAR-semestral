package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.exceptions.InvalidBodyException;
import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_DISCOUNT_404_EXAMPLE;

@Tag(name = "Discount API", description = "API for getting data about discount.")
@RequestMapping("/discounts")
public interface DiscountApi {

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @SecurityRequirement(name = "basicAuth")
    @Operation(

            summary =
                    "Updates discount parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK"),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Specified discount not found",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "Specified discount not found",
                                            summary = "Specified discount not found",
                                            description = "Specified discount not found.",
                                            value = RESPONSE_DISCOUNT_404_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = NotFoundException.class))),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Body not sent, or wrong body.",
                            content =
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = InvalidBodyException.class))),
            })
    void updateDiscount(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Discount information",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = DiscountDto.class,
                            example = """
                                    {
                                      "id": "1",
                                      "name": "SUMMER2023",
                                      "description": "Summer Discount",
                                      "discount": 0.25,
                                      "startDate": "2023-06-01",
                                      "expirationDate": "2023-09-01"
                                    }
                                    """),
                            examples = {
                            @ExampleObject(name = "Discount information",
                                    summary = "Discount information body",
                                    description = "Discount information body.",
                                    value = """
                                    {
                                      "id": "1",
                                      "name": "SUMMER2023",
                                      "description": "Summer Discount",
                                      "discount": 0.25,
                                      "startDate": "2023-06-01",
                                      "expirationDate": "2023-09-01"
                                    }
                                    """)}
                    )
            )
            @RequestBody DiscountDto discount);
}
