package cz.cvut.fel.ear.carrentalservice.model;

public record CarRentalDto(
        String name,
        String street,
        Integer streetNumber,
        String postal,
        String city
) {

}
