package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.exceptions.InvalidBodyException;
import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Map;

import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_DELETE_404_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_GET_VEHICLES_200_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_GET_VEHICLES_REG_200_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_GET_VEHICLES_REG_404_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_GET_VEHICLES_STATS_200_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_REPAIRS_404_EXAMPLE;

@Tag(name = "Vehicle API", description = "API for getting data about vehicles.")
@RequestMapping("/vehicles")
public interface VehicleApi {


    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Returns all vehicles. Can be paginated by specifying query parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getVehicles",
                                            summary = "Two Vehicles",
                                            description = "Two vehicles are in the DB.",
                                            value = RESPONSE_GET_VEHICLES_200_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = VehicleDto.class)))
            })
    List<VehicleDto> getVehicles(
            @Parameter(name = "orderBy", description = "Sets the order of the list. DESC or ASC.", example = "DESC") @RequestParam(required = false, defaultValue = "DESC") String orderBy,
            @Parameter(name = "offset", description = "Sets page offset.", example = "2") @RequestParam(required = false, defaultValue = "0") Integer offset,
            @Parameter(name = "limit", description = "Controls the page size.", example = "20") @RequestParam(required = false, defaultValue = "10") Integer limit);

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{registration}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Returns a vehicle. Returns more details about the vehicle (accidents, repairs, etc.) when allDetails is true.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getCarByRegistration",
                                            summary = "Returns vehicle example",
                                            description = "Vehicle with registration ABC123 is returned.",
                                            value = RESPONSE_GET_VEHICLES_REG_200_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = VehicleDto.class))),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Vehicle with given license not found.",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getCarNotFound",
                                            summary = "Vehicle hasn't been found",
                                            description = "Vehicle hasn't been found in the DB.",
                                            value = RESPONSE_GET_VEHICLES_REG_404_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = NotFoundException.class)))
            })
    VehicleDto getVehicleByRegistration(
            @Parameter(name = "registration", description = "Vehicle registration", example = "ABC123", required = true) @PathVariable String registration,
            @Parameter(name = "allDetails", description = "If true, entity contains more details such as Car rentals or registrations.", example = "true") @RequestParam(required = false, defaultValue = "false") Boolean allDetails);

    @ResponseStatus(HttpStatus.CREATED)
    @SecurityRequirement(name = "basicAuth")
    @PostMapping(value = "/{registration}/repairShops", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Adds a new repair to a vehicle with its corresponding repair shop.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "OK",
                            content =
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = RepairDto.class))),

                    @ApiResponse(
                            responseCode = "404",
                            description = "Vehicle with given license or repair shop with given name not found.",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getCarNotFound",
                                            summary = "Vehicle hasn't been found",
                                            description = "Vehicle with given license or repair shop with given name not found in the DB.",
                                            value = RESPONSE_REPAIRS_404_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = NotFoundException.class))),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Empty body",
                            content =
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = InvalidBodyException.class))),
                    @ApiResponse(
                            responseCode = "401",
                            description = "User doesn't have the privileges to call the endpoint.")
            })
    void addNewRepair(
            @Parameter(name = "registration", description = "Vehicle registration", example = "ABC123", required = true) @PathVariable String registration,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "New repair information",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = RepairDto.class,
                                    example = """
                                            {
                                              "repairShopName": "CarCare Garage",
                                              "serial": "1234",
                                              "name": "fix a servo",
                                              "description": "description",
                                              "date": "2023-11-13"
                                            }
                                            """),
                            examples = {
                                    @ExampleObject(name = "Discount information",
                                            summary = "Discount information body",
                                            description = "Discount information body.",
                                            value = """
                                                    {
                                                      "repairShopName": "CarCare Garage",
                                                      "serial": "1234",
                                                      "name": "fix a servo",
                                                      "description": "description",
                                                      "date": "2023-11-13"
                                                    }
                                                    """)}
                    )
            )
            @RequestBody RepairDto repairDto
    );

    @ResponseStatus(HttpStatus.OK)
    @SecurityRequirement(name = "basicAuth")
    @DeleteMapping(value = "/{registration}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Deletes a vehicle and performs a cascade delete on any of its other records (repairs, subscriptions).")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK"),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Vehicle with given license or repair shop with given name not found.",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "vehicle not found",
                                            summary = "vehicle not found",
                                            description = "vehicle not found",
                                            value = RESPONSE_DELETE_404_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = NotFoundException.class))),
                    @ApiResponse(
                            responseCode = "401",
                            description = "User doesn't have the privileges to call the endpoint.")})
    void deleteVehicleAndDataByLicense(@Parameter(name = "registration", description = "Vehicle registration", example = "ABC123", required = true) @PathVariable String registration);

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/repairStatistics", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Returns number of repairs based on vehicle brand.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getVehiclesRepairs number",
                                            summary = "number of repairs based on vehicle brand",
                                            description = "returns number of repairs based on vehicle brand",
                                            value = RESPONSE_GET_VEHICLES_STATS_200_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = VehicleDto.class)))
            })
    List<Map.Entry<String, Long>> getVehiclesRepairsStat();
}
