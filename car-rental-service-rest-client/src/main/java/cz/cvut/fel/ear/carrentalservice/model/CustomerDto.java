package cz.cvut.fel.ear.carrentalservice.model;


import java.time.LocalDate;
import java.util.List;

public record CustomerDto(
        String licenseCode,
        String firstName,
        String lastName,
        LocalDate birthdate,
        String phone,
        List<SubscriptionDto> subscriptions
) {}
