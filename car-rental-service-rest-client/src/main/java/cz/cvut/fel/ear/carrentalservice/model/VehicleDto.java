package cz.cvut.fel.ear.carrentalservice.model;


import cz.cvut.fel.ear.carrentalservice.enums.Condition;
import cz.cvut.fel.ear.carrentalservice.enums.FuelType;

import java.time.LocalDate;
import java.util.List;

public record VehicleDto(
        String vin,
        String registration,
        String model,
        String manufacturer,
        String bodyType,
        LocalDate productionDate,
        FuelType fuelType,
        Condition condition,
        Double price,
        CarRentalDto carRental,
        List<SubscriptionDto> subscriptions
        ) {
}
