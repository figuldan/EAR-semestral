package cz.cvut.fel.ear.carrentalservice.enums;

public enum FuelType {
    DIESEL,
    GAS,
    ELECTRIC,
    HYBRID
}
