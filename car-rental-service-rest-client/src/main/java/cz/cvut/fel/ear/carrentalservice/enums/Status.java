package cz.cvut.fel.ear.carrentalservice.enums;

public enum Status {
    AVAILABLE,
    IN_REPAIR,
    UNAVAILABLE
}
