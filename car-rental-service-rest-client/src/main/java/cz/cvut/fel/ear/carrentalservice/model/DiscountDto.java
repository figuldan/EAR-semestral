package cz.cvut.fel.ear.carrentalservice.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public record DiscountDto (

        String id,

        String name,

        String description,

        BigDecimal discount,

        LocalDate startDate,

        LocalDate expirationDate
) {

}
