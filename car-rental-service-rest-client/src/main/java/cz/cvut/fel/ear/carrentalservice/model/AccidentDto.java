package cz.cvut.fel.ear.carrentalservice.model;

import java.time.LocalDate;

public record AccidentDto(
        SubscriptionDto subscription,
        String name,
        String description,
        LocalDate date
) {

}
