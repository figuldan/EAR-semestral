package cz.cvut.fel.ear.carrentalservice.enums;

public enum Condition {
    EXCELLENT,
    GOOD,
    FAIR,
    POOR
}
