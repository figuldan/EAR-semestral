package cz.cvut.fel.ear.carrentalservice.model;

import java.time.LocalDate;

public record SubscriptionDto(
        LocalDate startDate,
        LocalDate endDate
) {
}
