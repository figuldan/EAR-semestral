package cz.cvut.fel.ear.carrentalservice.api.examples;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ExampleStrings {
    /////////////////////// DiscountAPI
    public static final String RESPONSE_DISCOUNT_404_EXAMPLE = """
            {
                "timestamp": "2023-11-13T07:57:41.609+00:00",
                "status": 404,
                "error": "Not Found",
                "message": "Requested discount with name: 1234 could not be found",
                "path": "/discounts"
            }
            """;

    /////////////////////// CustomerAPI
    public static final String RESPONSE_CUSTOMER_200_EXAMPLE = """
            [
                {
                    "subscription": {
                        "startDate": "2023-02-01",
                        "endDate": "2023-03-01"
                    },
                    "name": "Side Impact Crash",
                    "description": "Vehicle was hit on the side while turning at an intersection",
                    "date": "2023-02-01"
                }
            ]
            """;

    public static final String RESPONSE_CUSTOMER_404_EXAMPLE = """
            {
                "timestamp": "2023-11-13T07:36:57.620+00:00",
                "status": 404,
                "error": "Not Found",
                "message": "The customer with licenseCode 9c2d616fc-561d-4c95-b8c3-5f9a9d8301f9 hasn't been found",
                "path": "/customers/9c2d616fc-561d-4c95-b8c3-5f9a9d8301f9/accidents"
            }
            """;

    /////////////////////// VehicleAPI

    public static final String RESPONSE_GET_VEHICLES_STATS_200_EXAMPLE = """
            [
                {
                    "Honda": 22
                },
                {
                    "BMW": 0
                },
                {
                    "Skoda": 0
                },
                {
                    "Volvo": 0
                }
            ]
            """;
    public static final String RESPONSE_GET_VEHICLES_200_EXAMPLE = """
            [
                {
                    "vin": "1G1YZ2D76F5609604",
                    "registration": "VWX890",
                    "model": "Ridgeline",
                    "manufacturer": "Honda",
                    "bodyType": "Truck",
                    "productionDate": "2024-06-01",
                    "fuelType": "DIESEL",
                    "condition": "POOR",
                    "price": 40000.0,
                    "carRental": {
                        "name": "AccelerateNow Car Rentals",
                        "street": "Tennyson",
                        "streetNumber": 69,
                        "postal": "2329",
                        "city": "Leiden"
                    },
                    "subscriptions": []
                },
                {
                    "vin": "1G1YZ2D76F5609603",
                    "registration": "STU567",
                    "model": "Insight",
                    "manufacturer": "Honda",
                    "bodyType": "Sedan",
                    "productionDate": "2024-05-01",
                    "fuelType": "HYBRID",
                    "condition": "FAIR",
                    "price": 30000.0,
                    "carRental": {
                        "name": "DriveStar Auto Leasing",
                        "street": "Chinook",
                        "streetNumber": 4069,
                        "postal": "098765",
                        "city": "Afareaitu"
                    },
                    "subscriptions": []
                }
            ]
            """;

    public static final String RESPONSE_GET_VEHICLES_REG_404_EXAMPLE = """
            {
                "timestamp": "2023-11-13T09:13:07.715+00:00",
                "status": 404,
                "error": "Not Found",
                "message": "The vehicle of registration BBB hasn't been found",
                "path": "/vehicles/BBB"
            }
            """;

    public static final String RESPONSE_GET_VEHICLES_REG_200_EXAMPLE = """
            {
                "vin": "1G1YZ2D76F5609575",
                "registration": "ABC123",
                "model": "Civic",
                "manufacturer": "Honda",
                "bodyType": "Sedan",
                "productionDate": "2022-01-01",
                "fuelType": "GAS",
                "condition": "EXCELLENT",
                "price": 25000.0,
                "carRental": {
                    "name": "ZoomDrive Rentals",
                    "street": "Ruskin",
                    "streetNumber": 75312,
                    "postal": "16900",
                    "city": "Huarong"
                },
                "subscriptions": null
            }
            """;

    public static final String RESPONSE_REPAIRS_404_EXAMPLE = """
            {
                "timestamp": "2023-11-13T09:25:24.854+00:00",
                "status": 404,
                "error": "Not Found",
                "message": "Requested vehicle with registration: 123 not found",
                "path": "/vehicles/123/repairShops"
            }""";

    public static final String RESPONSE_DELETE_404_EXAMPLE = """
            {
                "timestamp": "2023-11-13T09:32:58.925+00:00",
                "status": 404,
                "error": "Not Found",
                "message": "Vehicle with registration: ABC123 not found",
                "path": "/vehicles/ABC123"
            }""";
}
