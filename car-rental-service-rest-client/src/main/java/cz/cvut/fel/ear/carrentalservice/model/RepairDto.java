package cz.cvut.fel.ear.carrentalservice.model;

import java.time.LocalDate;

public record RepairDto (

        String repairShopName,

        String serial,

        String name,

        String description,

        LocalDate date
){

}