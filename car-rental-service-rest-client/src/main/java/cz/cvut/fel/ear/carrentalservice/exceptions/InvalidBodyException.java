package cz.cvut.fel.ear.carrentalservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidBodyException extends ResponseStatusException {
    public InvalidBodyException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);

    }
}
