package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_CUSTOMER_200_EXAMPLE;
import static cz.cvut.fel.ear.carrentalservice.api.examples.ExampleStrings.RESPONSE_CUSTOMER_404_EXAMPLE;

@OpenAPIDefinition(info = @Info(title = "Car Rental Service API", version = "v1"))
@Tag(name = "Customer API", description = "API for getting data about customers.")
@RequestMapping("/customers")
public interface CustomerApi {


    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{licenseCode}/accidents", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary =
                    "Returns all accidents of a specific customer. Accidents can be fetched from a time interval specified by query parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getCustomerAccidents",
                                            summary = "User has one accident",
                                            description = "User has one accident from the time frame.",
                                                value = RESPONSE_CUSTOMER_200_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = AccidentDto.class))),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Customer with specified license code not found.",
                            content =
                            @Content(examples = {
                                    @ExampleObject(name = "getCustomerAccidents",
                                            summary = "User hasn't been found",
                                            description = "User hasn't been found in the DB.",
                                            value = RESPONSE_CUSTOMER_404_EXAMPLE)},
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = NotFoundException.class))),
            })
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    List<AccidentDto> getCustomerAccidents(
            @Parameter(name = "licenseCode", description = "Drivers license code of the user.", example = "886c5eda-4f4f-418f-8961-db5ee5743f45", required = true) @PathVariable String licenseCode,
            @Parameter(name = "from", description = "Parameter to specify the accidents search date", example = "2022-10-10") @Schema(name = "from", defaultValue = "LocalDate.EPOCH", example = "2022-10") @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> from,
            @Parameter(name = "to", description = "Parameter to specify the accidents search date", example = "2023-10-10") @Schema(name = "to", defaultValue = "LocalDate.now", example = "2023-10") @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> to
    );
}
