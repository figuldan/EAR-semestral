# Projects final report

Everything about projects structure, how to run the service, technologies used, documentation etc.
can be found in the [README](README.md) section.

## Our feedback

### Daniel Figuli
As I was already familiar with the technologies used in this subject, I did not learn much from it.
However, overall, I rate this semester project and the subject positively. 
For me, it was a nice refresher and good practice to build my own application from scratch. 
The topics covered in this subject were relevant to today's technologies, and I believe it 
provides a great introduction to developing enterprise-scaled applications.

### Martin Černý
I was already familiar with SpringBoot technology before I started the EAR course, 
so it didn't bring me much new knowledge. Nevertheless, I practiced working with SpringBoot, 
Flyway, Docker environment and I tried SpringBoot testing using isolated test containers. 
I think the course provides a good basic introduction to medium and larger software projects 
and the work involved.