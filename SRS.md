# Software Requirements Specification (SRS) - Car Rental Service

<!-- TOC -->
* [Software Requirements Specification (SRS) - Car Rental Service](#software-requirements-specification-srs---car-rental-service)
  * [1. Introduction](#1-introduction)
    * [1.1 Purpose](#11-purpose)
    * [1.2 Project Scope](#12-project-scope)
    * [1.3 Document conventions](#13-document-conventions)
  * [2. User Classes and Characteristics](#2-user-classes-and-characteristics)
    * [2.1 Car Rental service customer](#21-car-rental-service-customer)
    * [2.2 Car Rental service administrative user (ADMIN)](#22-car-rental-service-administrative-user-admin)
  * [3. Service functions:](#3-service-functions)
    * [3.4 Security](#34-security)
  * [4. Non-Functional Requirements](#4-non-functional-requirements)
    * [4.1 Description](#41-description)
      * [4.2 ER diagram](#42-er-diagram)
  * [5. Operating enviroment](#5-operating-enviroment)
  * [6. Dependencies](#6-dependencies)
  * [7. Assumptions and Constraints](#7-assumptions-and-constraints)
  * [8. Local Development](#8-local-development)
  * [9. Glossary](#9-glossary)
  * [10. Revision History](#10-revision-history)
<!-- TOC -->

## 1. Introduction

### 1.1 Purpose

The purpose of this document is to outline the software requirements for the Car Rental Service application. 
This application is built using Spring Boot 3, JPA Repositories, Maven, Mapstruct, and Lombok, 
and it interacts with a second service named Eshop service through REST API.

### 1.2 Project Scope

The Car Rental service allows customers to browse vehicles, view vehicle details, and perform various operations related to vehicles, repairs, and discounts.
Service also holds data about car repairs and repair shops and car subscriptions.
The application features RESTful API endpoints for retrieving, adding, updating, and deleting data.

### 1.3 Document conventions

| Abbreviation | Meaning                                                                                     |
|--------------|---------------------------------------------------------------------------------------------|
| REST API     | interface that two computer systems use to exchange information securely over the internet. |
| DB           | Database                                                                                    |
| ER           | Entity Relationship                                                                         |
| ADMIN        | Administrative user                                                                         |

## 2. User Classes and Characteristics

### 2.1 Car Rental service customer
Car rental service customer shall have access to the list of vehicles, 
their subscriptions, repairs and repairshops.
The customer shall be able to create subscriptions online.
The customer cannot modify the data, or send any other POST, PUT, or DELETE manually, with some business logic exceptions.

### 2.2 Car Rental service administrative user (ADMIN)
Car rental service administrative user shall have access to the production DB, when needed.
Admin shall be able to manually send any query they need to.
They shall try to keep the production deploy as stable as possible.

## 3. Service functions:
>### 3.1 Vehicle functionalities
> **The system shall offer an endpoint to retrieve all vehicles with optional pagination.**
>- Vehicles can be ordered based on a specified enumeration.
>- Pagination is supported with query parameters offset and limit, with default values set to 0 and 10 respectively.
>
> **The system shall provide an endpoint to retrieve details about a specific vehicle.**
>- Additional details about the vehicle, such as accidents and repairs, shall be included in the response if the query parameter allDetails is set to true.
>
> **The system shall provide a functionality to add a new repair to a specific vehicle, including details about the repair shop.**
>
> **The system shall provide an option to delete existing vehicle and perform a cascade delete on associated records (repairs, subscriptions).**
>- Makes use of cascade delete.

>### 3.2 Accidents functionalities
>
>#### **The system shall provide an endpoint to retrieve all accidents of a specific customer within a specified time interval.**
>-  Accidents can be fetched from the time interval specified by the query parameters from and to, 
with default values set to LocalDate.EPOCH and LocalDate.now respectively.

>### 3.3 Discount functionalities
>
>#### **The system shall provide a functionality to update discount parameters.**

### 3.4 Security

- The system shall implement **HTTP Basic** security for the `POST`, `PUT`, and `DELETE` endpoints.
- Users attempting to access sensitive operations must provide valid authentication credentials.

## 4. Non-Functional Requirements

### 4.1 Description

The Car Rental service makes a vehicle renting platform, with options to hold useful data about
the subscriptions, car rentals, repairs, accidents, etc.

#### 4.2 ER diagram

![](pic/diagram/car-rental.png "Diagram of the service classes.")
*Diagram of the service classes.*

## 5. Operating enviroment
The system is being developed using SpringBoot 3 and multiple dependencies as the backend (see the [dependencies](#6-dependencies) stated below).
The system shall be hosted locally for testing purposes.
The system is being designed following the MVC (Model View Controller) pattern and 
SpringBoot service framework pattern.

## 6. Dependencies

- The application uses several plugins, dependencies and frameworks: 
  - Spring Boot 3, 
  - JPA Repositories, 
  - Maven, 
  - Mapstruct, 
  - Lombok
  - Flyway for generating mock test data.

## 7. Assumptions and Constraints

- The application assumes a **properly configured DB** accessible to the application. Otherwise, the application will throw errors and 500 errors when accessing the endpoints. 
- **Flyway** is assumed to be used for DB migration and mock data generation. It runs on the first run of the application.
- Tests are assumed to be run **with Docker running**. Otherwise, the tests will not run. 
More information is in the README.md.

## 8. Local Development

- These steps are nessesary to run and debug the application on your local machine.
- Local development is described in [README.md](README.md).
- There is other useful information in the Readme.

## 9. Glossary

- **License Code:** Unique identifier for a customer's license.
- **Registration:** Unique identifier for a vehicle.
- **Serial** Unique identifier for a repair. It is a generated UUID.

## 10. Revision History

| Version | Date       | Description | Author       | 
|---------|------------|-------------|--------------|
| 1.0     | 2023-11-12 | Add SRS     | Martin Černý |