INSERT INTO public.T_CAR_RENTAL (id, name, street, street_number, postal, city)
VALUES (1, 'name', 'street', 0, 'postal', 'city');

INSERT INTO public.T_DISCOUNT (id, name, description, discount, start_date, expiration_date)
VALUES (1, 'name', 'description', 0.5, '2022-01-01', '2022-06-01');

INSERT INTO public.T_VEHICLE (id, vin, registration, model, manufacturer, body_type, production_date, fuel_type, condition,
                              status, price, car_rental_id, discount_id)
VALUES (1, 'vin', 'registration', 'model', 'manufacturer', 'bodyType', '2000-01-01', 'DIESEL'::fuel_type,
        'GOOD'::condition_type, 'AVAILABLE'::status, 20000.00, 1, 1);

INSERT INTO public.T_CUSTOMER (id, license_code, first_name, last_name, birthdate, phone)
VALUES (1,'licenseCode','firstName','lastName','2000-01-01','000-000-0000');

INSERT INTO public.T_SUBSCRIPTION (id, start_date, end_date, vehicle_id, customer_id)
VALUES (1, '2023-01-01', '2023-02-01', 1, 1);

INSERT INTO public.T_ACCIDENT (id, serial, name, description, date, subscription_id)
VALUES (1, '123456789', 'firstName', 'firstDescription', '2022-01-01', 1),
       (2, 'abcdefghi', 'secondName', 'secondDescription', '2022-02-01', 1);

INSERT INTO public.T_REPAIR_SHOP (id, name, street, street_number, postal, city, repair_type)
VALUES (1, 'name', 'street', 0, 'postal', 'city', 'GAS'::fuel_type);