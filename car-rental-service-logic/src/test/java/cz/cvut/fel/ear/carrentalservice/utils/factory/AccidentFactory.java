package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.util.List;

@UtilityClass
public class AccidentFactory {

    public List<AccidentDto> buildDefaultAccidents() {
        return List.of(
                new AccidentDto(
                        SubscriptionFactory.buildExtendedSubscription(),
                        "firstName",
                        "firstDescription",
                        LocalDate.of(2022, 1, 1)
                ),
                new AccidentDto(
                        SubscriptionFactory.buildExtendedSubscription(),
                        "secondName",
                        "secondDescription",
                        LocalDate.of(2022, 2, 1)
                )
        );
    }

    public List<AccidentDto> buildSingleAccident() {
        return List.of(
                new AccidentDto(
                        SubscriptionFactory.buildExtendedSubscription(),
                        "firstName",
                        "firstDescription",
                        LocalDate.of(2022, 1, 1)
                )
        );
    }

    public AccidentDto buildEmptyAccident() {
        return new AccidentDto(
                null,
                null,
                null,
                null
        );
    }
}
