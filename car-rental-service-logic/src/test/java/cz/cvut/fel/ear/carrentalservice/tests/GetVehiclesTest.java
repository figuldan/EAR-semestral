package cz.cvut.fel.ear.carrentalservice.tests;

import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import cz.cvut.fel.ear.carrentalservice.utils.QueryParams;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_extended.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class GetVehiclesTest extends TestDatabase {

    @Test
    @DisplayName("Get all vehicles with default params")
    void givenDefaults_whenGetAllVehicles_returnAllVehicles() {

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES)
                .build()
                .toUri();

        ResponseEntity<List<VehicleDto>> response = buildRequest(uri);

        List<VehicleDto> responseVehicles = Objects.requireNonNull(response.getBody());

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(responseVehicles).isNotEmpty(),
                () -> assertThat(responseVehicles).hasSize(10),
                () -> assertThat(responseVehicles).extracting(VehicleDto::productionDate).isSortedAccordingTo(Comparator.reverseOrder())
        );
    }

    @Test
    @DisplayName("Get all vehicles using limit")
    void givenLimitQueryParam_whenGetAllVehicles_returnVehiclesByLimit() {

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES)
                .queryParam(QueryParams.LIMIT, 20)
                .build()
                .toUri();

        ResponseEntity<List<VehicleDto>> response = buildRequest(uri);

        List<VehicleDto> responseVehicles = Objects.requireNonNull(response.getBody());

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(responseVehicles).isNotEmpty(),
                () -> assertThat(responseVehicles).hasSize(20)
        );
    }

    @Test
    @DisplayName("Get vehicles using offset")
    void givenOffsetParam_whenGetAllVehicles_returnVehiclesByOffset() {
        URI uri = UriComponentsBuilder
            .fromUriString(Endpoints.VEHICLES)
            .queryParam(QueryParams.OFFSET, 3)
            .build()
            .toUri();

        ResponseEntity<List<VehicleDto>> response = buildRequest(uri);

        List<VehicleDto> responseVehicles = Objects.requireNonNull(response.getBody());

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(responseVehicles).isEmpty()
        );
    }


    @Test
    @DisplayName("Get vehicles using sort")
    void givenSortingParam_whenGetAllVehicles_returnSortedVehicles() {
        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES)
                .queryParam(QueryParams.ORDER_BY, "ASC")
                .build()
                .toUri();

        ResponseEntity<List<VehicleDto>> response = buildRequest(uri);

        List<VehicleDto> responseVehicles = Objects.requireNonNull(response.getBody());

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(responseVehicles).isNotEmpty(),
                () -> assertThat(responseVehicles).hasSize(10),
                () -> assertThat(responseVehicles).extracting(VehicleDto::productionDate).isSortedAccordingTo(Comparator.naturalOrder())
        );
    }

        private ResponseEntity<List<VehicleDto>> buildRequest(URI uri) {
            return testRestTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {}
            );
        }

}
