package cz.cvut.fel.ear.carrentalservice;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Testcontainers
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class TestDatabase {

    @Value("${admin.username}")
    protected String adminUsername;

    @Value("${admin.password}")
    protected String adminPassword;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    private final static DockerImageName postgres= DockerImageName.parse("postgres:latest");

    @Container
    @SuppressWarnings("resource")
    public static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>(postgres)
            .withDatabaseName("car-rental")
            .withUsername("admin")
            .withPassword("admin")
            .withReuse(true);

    @DynamicPropertySource
    public static void postgreSQLDbProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        postgreSQLContainer.getFirstMappedPort();
    }

    @BeforeAll
    public static void initDatabase() {
        postgreSQLContainer.start();
    }

}
