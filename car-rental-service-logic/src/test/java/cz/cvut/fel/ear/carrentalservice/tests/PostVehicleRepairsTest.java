package cz.cvut.fel.ear.carrentalservice.tests;


import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.mappers.RepairMapper;
import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import cz.cvut.fel.ear.carrentalservice.persistence.RepairEntity;
import cz.cvut.fel.ear.carrentalservice.repository.RepairRepository;
import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import cz.cvut.fel.ear.carrentalservice.utils.factory.RepairFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_basic.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class PostVehicleRepairsTest extends TestDatabase {

    private static final String INVALID_REGISTRATION = "invalidRegistration";
    private static final String REGISTRATION = "registration";
    private static final String SERIAL = "serial";

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private RepairMapper repairMapper;

    @Test
    @DisplayName("Insert vehicle repair")
    void givenRepair_whenInsertingNewRepair_addsNewRepair() {
        RepairDto repairInput = RepairFactory.buildRepair();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REPAIR_SHOPS)
                .buildAndExpand(REGISTRATION)
                .toUri();

        ResponseEntity<RepairDto> response = buildRequest(uri, repairInput);

        RepairEntity createdRepairEntity = repairRepository.findBySerial(SERIAL)
                .orElse(null);

        RepairDto createdRepairDto = repairMapper.toDto(createdRepairEntity);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED),
                () -> assertThat(createdRepairDto).isNotNull(),
                () -> assertThat(createdRepairDto).usingRecursiveComparison().isEqualTo(repairInput)
        );
    }

    @Test
    @DisplayName("Insert repair with invalid registration")
    void givenRepair_whenInvalidVehicleRegistration_throwNotFoundException() {
        RepairDto repairInput = RepairFactory.buildRepair();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REPAIR_SHOPS)
                .buildAndExpand(INVALID_REGISTRATION)
                .toUri();

        ResponseEntity<RepairDto> response = buildRequest(uri, repairInput);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Insert repair with invalid repair shop name")
    void givenRepair_whenInvalidRepairShopName_throwNotFoundException() {
        RepairDto repairInput = RepairFactory.buildInvalidRepair();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REPAIR_SHOPS)
                .buildAndExpand(REGISTRATION)
                .toUri();

        ResponseEntity<RepairDto> response = buildRequest(uri, repairInput);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Insert repair with no admin credentials")
    void givenNoAdminCredentials_whenInsertingRepair_thenThrowUnauthorized() {
        RepairDto repairInput = RepairFactory.buildRepair();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REPAIR_SHOPS)
                .buildAndExpand(REGISTRATION)
                .toUri();

        ResponseEntity<RepairDto> response = buildRequestWithNoCredentials(uri, repairInput);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<RepairDto> buildRequestWithNoCredentials(URI uri, RepairDto repairInput) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return testRestTemplate
                .exchange(
                        uri,
                        HttpMethod.POST,
                        new HttpEntity<>(repairInput, headers),
                        RepairDto.class
                );
    }

    private ResponseEntity<RepairDto> buildRequest(URI uri, RepairDto repairInput) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return testRestTemplate
                .withBasicAuth(adminUsername, adminPassword).exchange(
                uri,
                HttpMethod.POST,
                new HttpEntity<>(repairInput, headers),
                RepairDto.class
        );
    }
}
