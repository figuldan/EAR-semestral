package cz.cvut.fel.ear.carrentalservice.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class QueryParams {

    public static final String LIMIT = "limit";

    public static final String OFFSET = "offset";

    public static final String ORDER_BY = "orderBy";

    public static final String ALL_DETAILS = "allDetails";

    public static final String FROM = "from";

    public static final String TO = "to";
}
