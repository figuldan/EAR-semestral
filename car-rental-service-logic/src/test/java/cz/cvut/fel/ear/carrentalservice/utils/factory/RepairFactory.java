package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;

@UtilityClass
public class RepairFactory {

    public RepairDto buildRepair() {
        return new RepairDto(
                "name",
                "serial",
                "repairName",
                "description",
                LocalDate.of(2022, 1, 1)
        );
    }

    public RepairDto buildInvalidRepair() {
        return new RepairDto(
                "invalidName",
                "serial",
                "repairName",
                "description",
                LocalDate.of(2022, 1, 1)
        );
    }
}
