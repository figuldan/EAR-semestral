package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.model.CarRentalDto;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CarRentalFactory {


    public CarRentalDto buildCarRental() {
        return new CarRentalDto(
                "name",
                "street",
                0,
                "postal",
                "city"
        );
    }
}
