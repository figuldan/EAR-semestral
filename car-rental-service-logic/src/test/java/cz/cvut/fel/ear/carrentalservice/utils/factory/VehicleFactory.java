package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.enums.Condition;
import cz.cvut.fel.ear.carrentalservice.enums.FuelType;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import lombok.experimental.UtilityClass;


import java.time.LocalDate;
import java.util.List;

@UtilityClass
public class VehicleFactory {

    public VehicleDto buildDefaultVehicle() {
        return new VehicleDto(
                "vin",
                "registration",
                "model",
                "manufacturer",
                "bodyType",
                LocalDate.of(2000, 1, 1),
                FuelType.DIESEL,
                Condition.GOOD,
                20000.00,
                CarRentalFactory.buildCarRental(),
                null
        );
    }

    public VehicleDto buildExtendedVehicle() {
        return new VehicleDto(
                "vin",
                "registration",
                "model",
                "manufacturer",
                "bodyType",
                LocalDate.of(2000, 1, 1),
                FuelType.DIESEL,
                Condition.GOOD,
                20000.00,
                CarRentalFactory.buildCarRental(),
                List.of(SubscriptionFactory.buildExtendedSubscription())
        );
    }

    public VehicleDto buildEmptyVehicle() {
        return new VehicleDto(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

}
