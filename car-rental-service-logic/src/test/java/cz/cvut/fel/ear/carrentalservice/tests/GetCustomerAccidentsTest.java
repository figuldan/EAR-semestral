package cz.cvut.fel.ear.carrentalservice.tests;

import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import cz.cvut.fel.ear.carrentalservice.utils.QueryParams;
import cz.cvut.fel.ear.carrentalservice.utils.factory.AccidentFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_basic.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class GetCustomerAccidentsTest extends TestDatabase {

    public static final String LICENSE_CODE = "licenseCode";

    public static final String INVALID_LICENSE_CODE = "invalidLicenseCode";

    @Test
    @DisplayName("Get customer accidents default")
    void givenDefaults_whenGetCustomerAccidents_returnCustomerAccidents() {

        List<AccidentDto> expectedResult = AccidentFactory.buildDefaultAccidents();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.CUSTOMER_ACCIDENTS)
                .buildAndExpand(LICENSE_CODE)
                .toUri();

        ResponseEntity<List<AccidentDto>> response = buildRequest(uri);

        List<AccidentDto> accidents = response.getBody();

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(accidents).isNotEmpty(),
                () -> assertThat(accidents).hasSameSizeAs(expectedResult),
                () -> assertThat(accidents).usingRecursiveComparison().isEqualTo(expectedResult)
        );
    }

    @Test
    @DisplayName("Get customer accidents from time interval")
    void givenTimeInterval_whenGetCustomerAccidents_returnAccidentsFromTimeInterval() {

        List<AccidentDto> expectedResult = AccidentFactory.buildSingleAccident();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.CUSTOMER_ACCIDENTS)
                .queryParam(QueryParams.FROM, LocalDate.of(2021, 12, 31))
                .queryParam(QueryParams.TO, LocalDate.of(2022, 1 , 31))
                .buildAndExpand(LICENSE_CODE)
                .toUri();

        ResponseEntity<List<AccidentDto>> response = buildRequest(uri);

        List<AccidentDto> accidents = response.getBody();

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(accidents).isNotEmpty(),
                () -> assertThat(accidents).hasSameSizeAs(expectedResult),
                () -> assertThat(accidents).usingRecursiveComparison().isEqualTo(expectedResult)
        );
    }

    @Test
    @DisplayName("Get customer by invalid license code")
    void givenInvalidLicense_whenGetCustomerAccidents_throwNotFoundException() {

        AccidentDto expectedResult = AccidentFactory.buildEmptyAccident();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.CUSTOMER_ACCIDENTS)
                .buildAndExpand(INVALID_LICENSE_CODE)
                .toUri();

        ResponseEntity<AccidentDto> response = buildInvalidRequest(uri);

        AccidentDto responseAccident = response.getBody();

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND),
                () -> assertThat(expectedResult).usingRecursiveComparison().isEqualTo(responseAccident)
        );

    }

    private ResponseEntity<List<AccidentDto>> buildRequest(URI uri) {
        return testRestTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {}
        );
    }

    private ResponseEntity<AccidentDto> buildInvalidRequest(URI uri) {
        return testRestTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {}
        );
    }
}
