package cz.cvut.fel.ear.carrentalservice.tests;


import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.mappers.DiscountMapper;
import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import cz.cvut.fel.ear.carrentalservice.persistence.DiscountEntity;
import cz.cvut.fel.ear.carrentalservice.repository.DiscountRepository;
import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import cz.cvut.fel.ear.carrentalservice.utils.factory.DiscountFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_basic.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class PutDiscountTest extends TestDatabase {

    private static final String NAME = "name";

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private DiscountMapper discountMapper;

    @Test
    @DisplayName("Put discount updates discount")
    void givenDiscountDto_whenPutDiscount_updateDiscount() {

        DiscountDto discountInput = DiscountFactory.buildDiscount();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.DISCOUNTS)
                .build()
                .toUri();

        ResponseEntity<DiscountDto> response = buildRequest(uri, discountInput);

        DiscountEntity discountEntity = discountRepository.findByName(discountInput.name())
                        .orElse(null);

        DiscountDto resultDto = discountMapper.toDto(discountEntity);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(resultDto).isNotNull(),
                () -> assertThat(discountInput).usingRecursiveComparison().isEqualTo(resultDto)
        );
    }

    @Test
    @DisplayName("Put discount with invalid repair shop name")
    void givenDiscount_whenInvalidRepairShopName_thenThrowNotFoundException() {

        DiscountDto discountInput = DiscountFactory.buildInvalidDiscount();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.DISCOUNTS)
                .build()
                .toUri();

        ResponseEntity<DiscountDto> response = buildRequest(uri, discountInput);

        DiscountEntity discountEntity = discountRepository.findByName(NAME)
                .orElse(null);

        DiscountDto resultDto = discountMapper.toDto(discountEntity);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND),
                () -> assertThat(discountInput).usingRecursiveComparison().isNotEqualTo(resultDto)
        );
    }

    @Test
    @DisplayName("Put discount with no admin credentials")
    void givenNoAdminCredentials_whenPuttingDiscount_thenThrowUnauthorized() {
        DiscountDto discountInput = DiscountFactory.buildInvalidDiscount();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.DISCOUNTS)
                .build()
                .toUri();

        ResponseEntity<DiscountDto> response = buildRequestWithNoCredentials(uri, discountInput);
        
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<DiscountDto> buildRequestWithNoCredentials(URI uri, DiscountDto discountInput) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return testRestTemplate.exchange(
                        uri,
                        HttpMethod.PUT,
                        new HttpEntity<>(discountInput, headers),
                        DiscountDto.class
                );

    }

    private ResponseEntity<DiscountDto> buildRequest(URI uri, DiscountDto discountInput) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return testRestTemplate
                .withBasicAuth(adminUsername, adminPassword).exchange(
                uri,
                HttpMethod.PUT,
                new HttpEntity<>(discountInput, headers),
                DiscountDto.class
        );
    }
}
