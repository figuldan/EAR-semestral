package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.model.SubscriptionDto;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;

@UtilityClass
public class SubscriptionFactory {

    public SubscriptionDto buildExtendedSubscription() {
        return new SubscriptionDto(
                LocalDate.of(2023, 1, 1),
                LocalDate.of(2023, 2, 1)
        );
    }
}
