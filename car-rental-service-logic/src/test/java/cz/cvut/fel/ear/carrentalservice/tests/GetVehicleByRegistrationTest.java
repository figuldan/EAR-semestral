package cz.cvut.fel.ear.carrentalservice.tests;

import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import cz.cvut.fel.ear.carrentalservice.utils.QueryParams;
import cz.cvut.fel.ear.carrentalservice.utils.factory.VehicleFactory;
import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_basic.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class GetVehicleByRegistrationTest extends TestDatabase {

    private static final String VALID_REGISTRATION = "registration";

    private static final String INVALID_REGISTRATION = "invalidRegistration";

    @Test
    @DisplayName("Get vehicle default params")
    void givenDefaults_whenGetVehicleByRegistration_returnVehicleByRegistration() {

        VehicleDto expectedDto = VehicleFactory.buildDefaultVehicle();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .queryParam(QueryParams.ALL_DETAILS, false)
                .buildAndExpand(VALID_REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequest(uri);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(expectedDto).usingRecursiveComparison().isEqualTo(response.getBody())
        );
    }

    @Test
    @DisplayName("Get vehicle with details")
    void givenAllDetailsParam_whenGetVehicleByRegistration_returnVehicleWithDetails() {

        VehicleDto expectedDto = VehicleFactory.buildExtendedVehicle();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .queryParam(QueryParams.ALL_DETAILS, true)
                .buildAndExpand(VALID_REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequest(uri);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(expectedDto).usingRecursiveComparison().isEqualTo(response.getBody())
        );
    }

    @Test
    @DisplayName("Get vehicle by invalid registration")
    void givenInvalidRegistration_whenGetVehicleByRegistration_throwException() {

        VehicleDto expectedDto = VehicleFactory.buildEmptyVehicle();

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .queryParam(QueryParams.ALL_DETAILS, false)
                .buildAndExpand(INVALID_REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequest(uri);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND),
                () -> assertThat(expectedDto).usingRecursiveComparison().isEqualTo(response.getBody())
        );
    }

    private ResponseEntity<VehicleDto> buildRequest(URI uri) {
        return testRestTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {}
        );
    }
}
