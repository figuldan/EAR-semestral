package cz.cvut.fel.ear.carrentalservice.utils.factory;

import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.time.LocalDate;

@UtilityClass
public class DiscountFactory {

    public DiscountDto buildDiscount() {
        return new DiscountDto(
                "1",
                "name",
                "newDescription",
                new BigDecimal("0.20"),
                LocalDate.of(2022, 3, 1),
                LocalDate.of(2022, 4, 1)
        );
    }

    public DiscountDto buildInvalidDiscount() {
        return new DiscountDto(
                "1",
                "invalidName",
                "newDescription",
                new BigDecimal("0.20"),
                LocalDate.of(2022, 3, 1),
                LocalDate.of(2022, 4, 1)
        );
    }
}
