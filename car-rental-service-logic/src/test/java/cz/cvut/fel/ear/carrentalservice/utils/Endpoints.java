package cz.cvut.fel.ear.carrentalservice.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Endpoints {
    public static final String VEHICLES_REGISTRATION = "/vehicles/{registration}";

    public static final String VEHICLES = "/vehicles";

    public static final String CUSTOMER_ACCIDENTS = "/customers/{licenseCode}/accidents";

    public static final String VEHICLES_REPAIR_SHOPS = "/vehicles/{registration}/repairShops";

    public static final String DISCOUNTS = "/discounts";
}
