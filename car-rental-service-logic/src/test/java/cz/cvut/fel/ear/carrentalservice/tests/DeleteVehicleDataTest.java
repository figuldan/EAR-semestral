package cz.cvut.fel.ear.carrentalservice.tests;

import cz.cvut.fel.ear.carrentalservice.TestDatabase;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import cz.cvut.fel.ear.carrentalservice.repository.AccidentRepository;
import cz.cvut.fel.ear.carrentalservice.repository.CarRentalRepository;
import cz.cvut.fel.ear.carrentalservice.repository.CustomerRepository;
import cz.cvut.fel.ear.carrentalservice.repository.DiscountRepository;
import cz.cvut.fel.ear.carrentalservice.repository.RepairShopRepository;
import cz.cvut.fel.ear.carrentalservice.repository.SubscriptionRepository;
import cz.cvut.fel.ear.carrentalservice.repository.VehicleRepository;
import cz.cvut.fel.ear.carrentalservice.utils.Endpoints;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@Sql(value = "/db/vehicle_data_basic.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/db/afterMigrate.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class DeleteVehicleDataTest extends TestDatabase {

    private static final Long DEFAULT_ID = 1L;

    private static final String REGISTRATION = "registration";

    private static final String INVALID_REGISTRATION = "invalidRegistration";

    @Autowired
    private CarRentalRepository carRentalRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RepairShopRepository repairShopRepository;

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private AccidentRepository accidentRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Test
    @DisplayName("Delete vehicle deletes all data")
    void givenRegistration_whenValidRegistration_deleteVehicleWithAllData() {

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .buildAndExpand(REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequest(uri);

        boolean deletedVehicle = vehicleRepository.existsById(String.valueOf(DEFAULT_ID));
        boolean deletedSubscription = subscriptionRepository.existsById(DEFAULT_ID);
        boolean deletedFirstAccident = accidentRepository.existsById(DEFAULT_ID);
        boolean deletedSecondAccident = accidentRepository.existsById(2L);

        boolean presentCustomer = customerRepository.existsById(String.valueOf(DEFAULT_ID));
        boolean presentRepairShop = repairShopRepository.existsById(DEFAULT_ID);
        boolean presentCarRental = carRentalRepository.existsById(DEFAULT_ID);
        boolean presentDiscount = discountRepository.existsById(DEFAULT_ID);


        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(deletedVehicle).isFalse(),
                () -> assertThat(deletedFirstAccident).isFalse(),
                () -> assertThat(deletedSecondAccident).isFalse(),
                () -> assertThat(deletedSubscription).isFalse(),
                () -> assertThat(presentCarRental).isTrue(),
                () -> assertThat(presentCustomer).isTrue(),
                () -> assertThat(presentRepairShop).isTrue(),
                () -> assertThat(presentDiscount).isTrue()
        );
    }

    @Test
    @DisplayName("Delete vehicle with invalid registration")
    void givenRegistration_whenInvalidRegistration_thenThrowNotFoundException() {

        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .buildAndExpand(INVALID_REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequest(uri);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Delete vehicle with no admin credentials")
    void givenNoAdminCredentials_whenDeleteVehicle_thenThrowUnauthorized() {
        URI uri = UriComponentsBuilder
                .fromUriString(Endpoints.VEHICLES_REGISTRATION)
                .buildAndExpand(REGISTRATION)
                .toUri();

        ResponseEntity<VehicleDto> response = buildRequestWithNoCredentials(uri);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<VehicleDto> buildRequestWithNoCredentials(URI uri) {
        return testRestTemplate.exchange(
                        uri,
                        HttpMethod.DELETE,
                        null,
                        new ParameterizedTypeReference<>() {}
                );
    }

    private ResponseEntity<VehicleDto> buildRequest(URI uri) {
        return testRestTemplate
                .withBasicAuth(adminUsername, adminPassword).exchange(
                uri,
                HttpMethod.DELETE,
                null,
                new ParameterizedTypeReference<>() {}
        );
    }

}
