CREATE TYPE fuel_type AS ENUM
(
    'DIESEL',
    'GAS',
    'ELECTRIC',
    'HYBRID'
);

CREATE TYPE condition_type AS ENUM
(
    'EXCELLENT',
    'GOOD',
    'FAIR',
    'POOR'
);

CREATE TYPE status AS ENUM
(
    'AVAILABLE',
    'IN_REPAIR',
    'UNAVAILABLE'
);

CREATE TABLE public.T_CAR_RENTAL
(
    id              BIGSERIAL       PRIMARY KEY,
    name            VARCHAR(255)    UNIQUE      NOT NULL,
    street          VARCHAR(255)                NOT NULL,
    street_number   INTEGER                     NOT NULL,
    postal          VARCHAR(255)                NOT NULL,
    city            VARCHAR (255)               NOT NULL
);

CREATE TABLE public.T_CUSTOMER
(
    id              BIGSERIAL       PRIMARY KEY,
    license_code    VARCHAR(255)    UNIQUE      NOT NULL,
    first_name      VARCHAR(255)                NOT NULL,
    last_name       VARCHAR(255)                NOT NULL,
    birthdate       DATE                        NOT NULL,
    phone           VARCHAR(255)                NOT NULL
);

CREATE TABLE public.T_REPAIR_SHOP
(
    id              BIGSERIAL       PRIMARY KEY,
    name            VARCHAR(255)    UNIQUE      NOT NULL,
    street          VARCHAR(255)                NOT NULL,
    street_number   INTEGER                     NOT NULL,
    postal          VARCHAR(255)                NOT NULL,
    city            VARCHAR (255)               NOT NULL,
    repair_type     fuel_type                   NOT NULL
);

CREATE TABLE public.T_DISCOUNT
(
    id              BIGSERIAL       PRIMARY KEY,
    name            VARCHAR(255)    UNIQUE      NOT NULL,
    description     TEXT                        NOT NULL,
    discount        NUMERIC(3, 2)               NOT NULL,
    start_date      DATE,
    expiration_date DATE

    CONSTRAINT percentage_check CHECK (discount >= 0 AND discount <= 1)
);

CREATE TABLE public.T_VEHICLE
(
    id              BIGSERIAL       PRIMARY KEY,
    vin             VARCHAR(255)    UNIQUE      NOT NULL,
    registration    VARCHAR(255)    UNIQUE      NOT NULL,
    model           VARCHAR(255)                NOT NULL,
    manufacturer    VARCHAR(255)                NOT NULL,
    body_type       VARCHAR(255)                NOT NULL,
    production_date DATE                        NOT NULL,
    fuel_type       fuel_type                   NOT NULL,
    condition       condition_type              NOT NULL,
    status          status                      NOT NULL,
    price           NUMERIC                     NOT NULL,
    car_rental_id   BIGINT                      NOT NULL,
    discount_id     BIGINT                      NOT NULL,

    CONSTRAINT car_rental_fk FOREIGN KEY (car_rental_id) REFERENCES t_car_rental(id),
    CONSTRAINT discount_fk FOREIGN KEY (discount_id) REFERENCES t_discount(id)
);

CREATE TABLE public.T_SUBSCRIPTION
(
    id              BIGSERIAL       PRIMARY KEY,
    start_date      DATE                        NOT NULL,
    end_date        DATE                        NOT NULL,
    vehicle_id      BIGINT                      NOT NULL,
    customer_id     BIGINT                      NOT NULL,

    CONSTRAINT vehicle_fk FOREIGN KEY (vehicle_id) REFERENCES t_vehicle(id),
    CONSTRAINT customer_fk FOREIGN KEY (customer_id) REFERENCES t_customer(id)
);

CREATE TABLE public.T_REPAIR
(
    id              BIGSERIAL       PRIMARY KEY,
    serial          VARCHAR(255)    UNIQUE      NOT NULL,
    name            VARCHAR(255)                NOT NULL,
    description     TEXT                        NOT NULL,
    date            DATE                        NOT NULL,
    vehicle_id      BIGINT                      NOT NULL,
    repair_shop_id  BIGINT                      NOT NULL,
        
    CONSTRAINT vehicle_fk FOREIGN KEY (vehicle_id) REFERENCES t_vehicle(id),
    CONSTRAINT repair_shop_fk FOREIGN KEY (repair_shop_id) REFERENCES t_repair_shop(id)
);

CREATE TABLE public.T_ACCIDENT
(
    id              BIGSERIAL       PRIMARY KEY,
    serial          VARCHAR(255)    UNIQUE      NOT NULL,
    name            VARCHAR(255)                NOT NULL,
    description     TEXT                        NOT NULL,
    date            DATE                        NOT NULL,
    subscription_id BIGINT                      NOT NULL,
    
    CONSTRAINT subscription_fk FOREIGN KEY (subscription_id) REFERENCES t_subscription(id)
);