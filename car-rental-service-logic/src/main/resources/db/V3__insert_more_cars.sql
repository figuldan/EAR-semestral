INSERT INTO T_VEHICLE (vin, registration, model, manufacturer, body_type, production_date, fuel_type, condition, status,
                       price, car_rental_id, discount_id)
VALUES ('1G1YZ2D76F5699999', 'AHJJ33', 'Civic', 'Skoda', 'Sedan', '2022-01-01', 'GAS'::fuel_type,
        'EXCELLENT'::condition_type, 'AVAILABLE'::status, 25000, 10, 1),
       ('1G1YZ2D76F5699998', 'AHJJ22', 'Accord', 'Skoda', 'Sedan', '2022-02-01', 'DIESEL'::fuel_type,
        'GOOD'::condition_type, 'AVAILABLE'::status, 27000, 2, 2),
       ('1G1YZ2D76F5499997', 'AHJJ11', 'CR-V', 'Skoda', 'SUV', '2022-03-01', 'ELECTRIC'::fuel_type,
        'FAIR'::condition_type, 'IN_REPAIR'::status, 30000, 3, 3),
       ('1G1YZ2D76F5699988', 'BMW311', 'Accord', 'BMW', 'Sedan', '2022-02-01', 'DIESEL'::fuel_type,
        'GOOD'::condition_type, 'AVAILABLE'::status, 27000, 2, 2),
       ('1G1YZ2D76F5499987', 'VOL311', 'CR-V', 'Volvo', 'SUV', '2022-03-01', 'ELECTRIC'::fuel_type,
        'FAIR'::condition_type, 'IN_REPAIR'::status, 30000, 3, 3);

INSERT INTO T_REPAIR (serial, name, description, date, vehicle_id, repair_shop_id)
VALUES ('SER143', 'Engine Check', 'Routine engine check and maintenance', '2023-01-01', 16, 1),
       ('SER144', 'Tire Replacement', 'Replacing worn-out tires with new ones', '2023-02-01', 17, 2);