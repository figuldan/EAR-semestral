package cz.cvut.fel.ear.carrentalservice.persistence;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Table(name = "T_CAR_RENTAL")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class CarRentalEntity extends BuildingEntity{

    @ToString.Exclude
    @OneToMany(mappedBy = "carRental", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<VehicleEntity> vehicleList;
}
