package cz.cvut.fel.ear.carrentalservice.security;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

@UtilityClass
public class SecurityEndpoints {

    final RequestMatcher SECURITY_URLS = new OrRequestMatcher(
            antMatcher(HttpMethod.PUT,"/discounts"),
            antMatcher(HttpMethod.DELETE,"/vehicles/{licenseCode}"),
            antMatcher(HttpMethod.POST,"/vehicles/{registration}/repairShops")
    );
    final RequestMatcher PERMITTED_URLS = new OrRequestMatcher(
            antMatcher(HttpMethod.GET,"/api-docs/**"),
            antMatcher(HttpMethod.GET,"/vehicles"),
            antMatcher(HttpMethod.GET,"/vehicles/{registration}"),
            antMatcher(HttpMethod.GET,"/customers/{licenseCode}/accidents")
    );
}
