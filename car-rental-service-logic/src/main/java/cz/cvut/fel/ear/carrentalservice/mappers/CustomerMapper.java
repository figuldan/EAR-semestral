package cz.cvut.fel.ear.carrentalservice.mappers;

import cz.cvut.fel.ear.carrentalservice.model.CustomerDto;
import cz.cvut.fel.ear.carrentalservice.persistence.CustomerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    CustomerDto toDto(CustomerEntity customerEntity);
}
