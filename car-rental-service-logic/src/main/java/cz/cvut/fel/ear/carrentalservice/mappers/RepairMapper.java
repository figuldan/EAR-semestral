package cz.cvut.fel.ear.carrentalservice.mappers;

import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import cz.cvut.fel.ear.carrentalservice.persistence.RepairEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RepairMapper {

    RepairEntity toEntity(RepairDto repairInput);

    @Mapping(target = "repairShopName", source = "repairShop.name")
    RepairDto toDto(RepairEntity repairEntity);

}
