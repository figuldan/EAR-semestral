package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import cz.cvut.fel.ear.carrentalservice.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RequestMapping("/customers")
@RestController
public class CustomerApiImpl implements CustomerApi {
    private final CustomerService customerService;

    @Override
    public List<AccidentDto> getCustomerAccidents(String licenseCode, Optional<LocalDate> from, Optional<LocalDate> to) {
        return customerService.getCustomerAccidents(licenseCode, from.orElse(LocalDate.EPOCH), to.orElse(LocalDate.now()));
    }
}

