package cz.cvut.fel.ear.carrentalservice.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "T_REPAIR")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class RepairEntity extends IncidentEntity {

    @ManyToOne
    @JoinColumn(name = "repair_shop_id")
    private RepairShopEntity repairShop;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private VehicleEntity vehicle;

}
