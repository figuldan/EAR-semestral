package cz.cvut.fel.ear.carrentalservice.mappers;

import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import cz.cvut.fel.ear.carrentalservice.persistence.AccidentEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AccidentMapper {
    AccidentDto toDto(AccidentEntity accidentEntity);

    List<AccidentDto> toDtos(List<AccidentEntity> accidentEntities);
}
