package cz.cvut.fel.ear.carrentalservice.persistence;

import cz.cvut.fel.ear.carrentalservice.enums.Condition;
import cz.cvut.fel.ear.carrentalservice.enums.FuelType;
import cz.cvut.fel.ear.carrentalservice.enums.Status;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@NamedQuery(
        name = "VehicleEntity.findAllGroupedByManufacturer",
        query = "SELECT v.manufacturer, COALESCE(COUNT(r), 0) FROM VehicleEntity v LEFT JOIN v.repair r GROUP BY v.manufacturer ORDER BY COALESCE(COUNT(r), 0) DESC"
)
@Table(name = "T_VEHICLE")
@Entity
@Data
@NoArgsConstructor
public class VehicleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "vin", nullable = false, unique = true)
    private String vin;

    @Column(name = "registration", nullable = false, unique = true)
    private String registration;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "manufacturer", nullable = false)
    private String manufacturer;

    @Column(name = "body_type", nullable = false)
    private String bodyType;

    @Column(name = "production_date", nullable = false)
    private LocalDate productionDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "fuel_type", nullable = false)
    private FuelType fuelType;

    @Enumerated(EnumType.STRING)
    @Column(name = "condition", nullable = false)
    private Condition condition;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "price", scale = 2, nullable = false)
    private Double price;

    @ManyToOne
    @JoinColumn(name = "car_rental_id")
    private CarRentalEntity carRental;

    @ManyToOne
    @JoinColumn(name = "discount_id")
    private DiscountEntity discount;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vehicle", cascade = CascadeType.REMOVE)
    @OrderBy("startDate ASC")
    private List<SubscriptionEntity> subscriptions;

    @ToString.Exclude
    @OneToMany(mappedBy = "vehicle", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<RepairEntity> repair;

}
