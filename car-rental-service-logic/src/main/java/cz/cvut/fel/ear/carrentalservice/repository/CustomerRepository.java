package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, String> {
    Optional<CustomerEntity> getCustomerEntityByLicenseCode(String licenseCode);
}
