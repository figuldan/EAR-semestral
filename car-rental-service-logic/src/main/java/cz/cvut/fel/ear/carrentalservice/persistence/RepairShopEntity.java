package cz.cvut.fel.ear.carrentalservice.persistence;

import cz.cvut.fel.ear.carrentalservice.enums.FuelType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "T_REPAIR_SHOP")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class RepairShopEntity extends BuildingEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "repair_type", nullable = false)
    private FuelType repairType;

}

