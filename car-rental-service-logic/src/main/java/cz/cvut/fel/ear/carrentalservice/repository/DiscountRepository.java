package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.DiscountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DiscountRepository extends JpaRepository<DiscountEntity, Long> {

    Optional<DiscountEntity> findByName(String name);
}
