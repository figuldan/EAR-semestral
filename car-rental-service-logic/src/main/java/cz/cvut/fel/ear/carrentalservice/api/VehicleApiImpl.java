package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.exceptions.InvalidBodyException;
import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import cz.cvut.fel.ear.carrentalservice.service.RepairService;
import cz.cvut.fel.ear.carrentalservice.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/vehicles")
@RequiredArgsConstructor
public class VehicleApiImpl implements VehicleApi {

    private final RepairService repairService;

    private final VehicleService vehicleService;

    @Override
    public List<VehicleDto> getVehicles(String orderBy, Integer offset, Integer limit) {
        return vehicleService.getVehicles(limit, orderBy, offset);
    }

    @Override
    public VehicleDto getVehicleByRegistration(String registration, Boolean allDetails) {
        return vehicleService.getVehicleByRegistration(registration, allDetails);
    }

    @Override
    public void addNewRepair(String registration, RepairDto repairDto) {
        if(repairDto.repairShopName() == null || repairDto.name() == null || repairDto.description() == null || repairDto.serial() == null){
            throw new InvalidBodyException("No or wrong body was sent");
        }
        repairService.addNewRepair(registration, repairDto);
    }

    @Override
    public void deleteVehicleAndDataByLicense(String registration) {
        vehicleService.deleteVehicleAndDataByLicense(registration);
    }

    @Override
    public List<Map.Entry<String, Long>> getVehiclesRepairsStat() {
        return repairService.getCarBrandsWithRepairCounts();
    }
}
