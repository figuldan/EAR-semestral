package cz.cvut.fel.ear.carrentalservice.service;

import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.mappers.VehicleMapper;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import cz.cvut.fel.ear.carrentalservice.persistence.VehicleEntity;
import cz.cvut.fel.ear.carrentalservice.repository.VehicleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class VehicleService {

    private final VehicleRepository vehicleRepository;

    private final VehicleMapper vehicleMapper;

    public VehicleDto getVehicleByRegistration(String registration, Boolean allDetails) {

        VehicleEntity vehicleEntity = vehicleRepository.getVehicleEntityByRegistration(registration)
                .orElseThrow(() -> new NotFoundException(String.format("The vehicle of registration %s hasn't been found", registration)));
        log.info("Returning a vehicle with registration {}.", registration);
        if (Boolean.TRUE.equals(!allDetails)) {
            vehicleEntity.setSubscriptions(null);
        }
        return vehicleMapper.toDto(vehicleEntity);
    }

    public List<VehicleDto> getVehicles(Integer limit, String orderBy, Integer offset) {
        log.info("Returning all vehicles");
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(orderBy), "productionDate"));
        List<VehicleEntity> vehicleEntities = vehicleRepository.findAll(pageable).getContent();
        return vehicleMapper.toDtos(vehicleEntities);
    }

    public void deleteVehicleAndDataByLicense(String registration) {
        vehicleRepository.findByRegistration(registration)
                .ifPresentOrElse(vehicle -> vehicleRepository.deleteByRegistration(registration),
                        () -> {throw new NotFoundException("Vehicle with registration: %s not found".formatted(registration));});
        log.info("Vehicle with registration: %s deleted with all other related data".formatted(registration));
    }
}
