package cz.cvut.fel.ear.carrentalservice.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "T_ACCIDENT")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class AccidentEntity extends IncidentEntity {

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "subscription_id")
    private SubscriptionEntity subscription;

}
