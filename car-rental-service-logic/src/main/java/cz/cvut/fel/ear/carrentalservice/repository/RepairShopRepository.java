package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.RepairShopEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RepairShopRepository extends JpaRepository<RepairShopEntity, Long> {

    Optional<RepairShopEntity> findByName (String name);
}
