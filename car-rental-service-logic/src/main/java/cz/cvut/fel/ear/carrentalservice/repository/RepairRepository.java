package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.RepairEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RepairRepository extends JpaRepository<RepairEntity, Long> {

    Optional<RepairEntity> findBySerial(String serial);

}
