package cz.cvut.fel.ear.carrentalservice.mappers;

import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import cz.cvut.fel.ear.carrentalservice.persistence.DiscountEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface DiscountMapper {

    @Mapping(target = "vehicleList", ignore = true)
    @Mapping(target = "id", ignore = true)
    void updateEntity(DiscountDto discountInput, @MappingTarget DiscountEntity discountEntity);

    DiscountDto toDto(DiscountEntity discountEntity);
}
