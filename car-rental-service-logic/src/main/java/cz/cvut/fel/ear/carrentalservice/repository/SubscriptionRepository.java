package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.SubscriptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends JpaRepository<SubscriptionEntity, Long> {
}
