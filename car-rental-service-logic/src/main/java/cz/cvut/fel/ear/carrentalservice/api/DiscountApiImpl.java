package cz.cvut.fel.ear.carrentalservice.api;

import cz.cvut.fel.ear.carrentalservice.exceptions.InvalidBodyException;
import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import cz.cvut.fel.ear.carrentalservice.service.DiscountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/discounts")
@RequiredArgsConstructor
public class DiscountApiImpl implements DiscountApi {

    private final DiscountService discountService;

    @Override
    public void updateDiscount(DiscountDto discountDto) {
        if (discountDto.discount() == null || discountDto.name() == null || discountDto.description() == null || discountDto.id() == null) {
            throw new InvalidBodyException("No or wrong body was sent");
        }
        discountService.updateDiscount(discountDto);
    }
}
