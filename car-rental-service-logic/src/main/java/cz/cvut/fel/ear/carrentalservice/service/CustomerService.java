package cz.cvut.fel.ear.carrentalservice.service;

import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.mappers.AccidentMapper;
import cz.cvut.fel.ear.carrentalservice.model.AccidentDto;
import cz.cvut.fel.ear.carrentalservice.persistence.AccidentEntity;
import cz.cvut.fel.ear.carrentalservice.persistence.CustomerEntity;
import cz.cvut.fel.ear.carrentalservice.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final AccidentMapper accidentMapper;

    public List<AccidentDto> getCustomerAccidents(String licenseCode, LocalDate from, LocalDate to) {
        CustomerEntity customerEntity = customerRepository
                .getCustomerEntityByLicenseCode(licenseCode)
                .orElseThrow(() -> new NotFoundException(String.format("The customer with licenseCode %s hasn't been found", licenseCode)));
        List<AccidentEntity> accidentEntities = customerEntity
                .getSubscriptions().stream()
                .flatMap(subscriptionEntity -> subscriptionEntity
                        .getAccidents().stream())
                .filter(accidentEntity -> accidentEntity
                        .getDate().isAfter(from) && accidentEntity
                        .getDate().isBefore(to))
                .toList();
        log.debug("Returning a customer accidents with licenseCode {}, from date {} to date {}.", licenseCode, from, to);
        return accidentMapper.toDtos(accidentEntities);
    }
}
