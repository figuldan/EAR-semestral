package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.CarRentalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRentalRepository extends JpaRepository<CarRentalEntity, Long> {
}
