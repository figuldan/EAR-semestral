package cz.cvut.fel.ear.carrentalservice.mappers;

import cz.cvut.fel.ear.carrentalservice.model.SubscriptionDto;
import cz.cvut.fel.ear.carrentalservice.model.VehicleDto;
import cz.cvut.fel.ear.carrentalservice.persistence.SubscriptionEntity;
import cz.cvut.fel.ear.carrentalservice.persistence.VehicleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring")
public interface VehicleMapper {

    @Mapping(target = "carRental", source = "carRental")
    VehicleDto toDto(VehicleEntity vehicleEntity);

    List<VehicleDto> toDtos(List<VehicleEntity> vehicleEntities);

    SubscriptionDto toSubscriptionDto(SubscriptionEntity subscriptionEntity);

    @Mapping(target = "vehicle", ignore = true)
    @Mapping(target = "accidents", ignore = true)
    List<SubscriptionDto> toSubscriptionDtos(List<SubscriptionEntity> subscriptionEntities);
}
