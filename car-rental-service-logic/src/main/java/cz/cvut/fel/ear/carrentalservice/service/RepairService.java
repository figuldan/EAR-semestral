package cz.cvut.fel.ear.carrentalservice.service;

import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.mappers.RepairMapper;
import cz.cvut.fel.ear.carrentalservice.model.RepairDto;
import cz.cvut.fel.ear.carrentalservice.persistence.RepairEntity;
import cz.cvut.fel.ear.carrentalservice.persistence.RepairShopEntity;
import cz.cvut.fel.ear.carrentalservice.persistence.VehicleEntity;
import cz.cvut.fel.ear.carrentalservice.repository.RepairRepository;
import cz.cvut.fel.ear.carrentalservice.repository.RepairShopRepository;

import cz.cvut.fel.ear.carrentalservice.repository.VehicleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class RepairService {

    private final RepairMapper repairMapper;

    private final RepairShopRepository repairShopRepository;
    private final VehicleRepository vehicleRepository;
    private final RepairRepository repairRepository;

    public void addNewRepair(String registration, RepairDto repairDto) {
        String repairShopName = repairDto.repairShopName();
        VehicleEntity vehicleEntity = vehicleRepository.findByRegistration(registration)
                .orElseThrow(() -> new NotFoundException("Requested vehicle with registration: %s not found".formatted(registration)));

        RepairShopEntity repairShopEntity = repairShopRepository.findByName(repairShopName)
                .orElseThrow(() -> new NotFoundException("Requested repair shop with name: %s not found".formatted(repairShopName)));

        RepairEntity repairEntity = repairMapper.toEntity(repairDto);
        repairEntity.setRepairShop(repairShopEntity);
        repairEntity.setVehicle(vehicleEntity);

        repairRepository.save(repairEntity);
        log.info("New repair for vehicle with registration: %s and repair shop name: %s saved".formatted(registration, repairShopName));
    }

    public List<Map.Entry<String, Long>> getCarBrandsWithRepairCounts() {
        List<Object[]> groupedVehiclesByManufacturer = vehicleRepository.findAllGroupedByManufacturer();

        return groupedVehiclesByManufacturer.stream()
                .map(entry -> (Map.Entry<String, Long>)
                        new AbstractMap.SimpleEntry<>((String) entry[0], (Long) entry[1]))
                .toList();

    }
}
