package cz.cvut.fel.ear.carrentalservice.service;

import cz.cvut.fel.ear.carrentalservice.exceptions.NotFoundException;
import cz.cvut.fel.ear.carrentalservice.mappers.DiscountMapper;
import cz.cvut.fel.ear.carrentalservice.model.DiscountDto;
import cz.cvut.fel.ear.carrentalservice.persistence.DiscountEntity;
import cz.cvut.fel.ear.carrentalservice.repository.DiscountRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DiscountService {

    private final DiscountRepository discountRepository;

    private final DiscountMapper discountMapper;

    public void updateDiscount(@NonNull DiscountDto discountDto) {

        String discountName = discountDto.name();
        DiscountEntity discountEntity = discountRepository.findByName(discountName)
                .orElseThrow(() -> new NotFoundException("Requested discount with name: %s could not be found".formatted(discountName)));
        discountMapper.updateEntity(discountDto, discountEntity);
        discountRepository.save(discountEntity);
        log.info("Discount with name: %s was updated".formatted(discountName));
    }
}
