package cz.cvut.fel.ear.carrentalservice.repository;

import cz.cvut.fel.ear.carrentalservice.persistence.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<VehicleEntity, String> {

    Optional<VehicleEntity> getVehicleEntityByRegistration(String registration);

    Optional<VehicleEntity> findByRegistration(String registration);

    List<Object[]> findAllGroupedByManufacturer();

    void deleteByRegistration(String registration);
}
