FROM maven:3.8.5-openjdk-17
EXPOSE 8080
COPY car-rental-service-logic/target/car-rental-service-logic-1.0.0.jar car-rental-service-logic-1.0.0.jar
ENTRYPOINT ["java", "-jar", "/car-rental-service-logic-1.0.0.jar"]